import fs from "fs/promises";
import path from "node:path";
import {
  SEED_FILE_NAME,
  ADDRESSES_FILE_NAME,
  WORK_DIR,
} from "./src/constants.js";
import { generateAddresses } from "./src/derivationUtils.js";

const getMnemonic = async () => {
  if (process.env.SEED_PHRASE) {
    return process.env.SEED_PHRASE;
  }
  const buffer = await fs.readFile(path.resolve(WORK_DIR, SEED_FILE_NAME));
  return buffer.toString("utf-8");
};

const main = async () => {
  const mnemonic = await getMnemonic();
  const addresses = generateAddresses(mnemonic);

  await fs.writeFile(
    path.resolve(WORK_DIR, ADDRESSES_FILE_NAME),
    JSON.stringify(addresses, null, 2),
  );
};

main()
  .then(() => console.log(`Addresses generated to "${ADDRESSES_FILE_NAME}"`))
  .catch((e) => {
    console.error("Failed to generate addresses", e);
    process.exit(-1);
  });
