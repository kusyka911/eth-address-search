import { BIP32Factory } from "bip32";
import * as bip39 from "bip39";
import * as ecc from "tiny-secp256k1";
import { privateKeyToAddress } from "web3-eth-accounts";
import { DERIVATION_ROTATIONS } from "./constants.js";

const bip32 = BIP32Factory(ecc);

/* 
ETH keys derivations paths to first address
BIP32 (blockchain.info) m/44'/0'/0'/0
BIP32 (ledger/coinomi) m/44'/60'/0'/0
BIP32 (multibit) m/0'/0/0
BIP44 m/44'/60'/0'/0/0
BIP49 m/49'/60'/0'/0/0
BIP84 m/84'/60'/0'/0/0
*/

const rotations = DERIVATION_ROTATIONS;

const getBlockchainInfoPaths = () => {
  const paths = [];
  for (let i = 0; i < rotations; i++) {
    for (let k = 0; k < rotations; k++) {
      paths.push(`m/44'/0'/${i}'/${k}`);
    }
  }
  return paths;
};

const getLedgerPaths = () => {
  const paths = [];
  for (let i = 0; i < rotations; i++) {
    for (let k = 0; k < rotations; k++) {
      paths.push(`m/44'/60'/${i}'/${k}`);
    }
  }
  return paths;
};

const getMultibitPaths = () => {
  const paths = [];
  for (let i = 0; i < rotations; i++) {
    for (let k = 0; k < rotations; k++) {
      paths.push(`m/0'/${i}/${k}`);
    }
  }
  return paths;
};

const getBipPaths = (bip = 44) => {
  const paths = [];
  for (let i = 0; i < rotations; i++) {
    for (let k = 0; k < rotations; k++) {
      paths.push(`m/${bip}'/60'/0'/${i}/${k}`);
    }
  }
  return paths;
};

/** @type {string[]} */
const derivationPaths = [].concat(
  getBlockchainInfoPaths(),
  getLedgerPaths(),
  getMultibitPaths(),
  getBipPaths(44),
  getBipPaths(49),
  getBipPaths(84),
);

export const generateAddresses = (mnemonic) => {
  const entropyBuffer = bip39.mnemonicToSeedSync(mnemonic);
  const root = bip32.fromSeed(entropyBuffer);
  return derivationPaths.map((p) => {
    const derivedNode = root.derivePath(p);
    const ethAccountAddress = privateKeyToAddress(
      new Uint8Array(derivedNode.privateKey),
    );
    return {
      derivationPath: p,
      ethAddress: ethAccountAddress,
    };
  });
};
