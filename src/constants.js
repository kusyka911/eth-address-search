export const SEED_FILE_NAME = "_seed.txt";
export const ADDRESSES_FILE_NAME = "_addresses.json";
export const OUTPUT_FILE_NAME = "_results.json";
export const WORK_DIR = process.cwd();
export const ETH_RPC_URL =
  process.env.ETH_RPC_URL || "wss://ethereum.publicnode.com";

const rotationsNum = parseInt(process.env.DERIVATION_ROTATIONS);

export const DERIVATION_ROTATIONS =
  rotationsNum && Number.isNaN(rotationsNum) ? 5 : rotationsNum;
