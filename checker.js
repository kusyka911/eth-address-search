import Web3 from "web3";
import { readFile, writeFile } from "node:fs/promises";
import path from "node:path";
import {
  ADDRESSES_FILE_NAME,
  OUTPUT_FILE_NAME,
  WORK_DIR,
} from "./src/constants.js";
import { string } from "zod";

const web3 = new Web3("wss://ethereum.publicnode.com");

const getAddresses = async () => {
  try {
    const buffer = await readFile(path.resolve(WORK_DIR, ADDRESSES_FILE_NAME));
    const strData = buffer.toString("utf-8");
    const data = JSON.parse(strData);
    // TODO: verify data
    return data;
  } catch (error) {
    console.error(error);
    console.error("failed to read addresses");
    process.exit(-1);
  }
};

const main = async () => {
  /** @type {{ derivationPath: string, ethAddress: string }[]} */
  const addresses = await getAddresses();

  // const batcher = new web3.eth.BatchRequest();

  const resultsPromise = Promise.all(
    addresses.map(async (a) => {
      try {
        const ethBalance = await web3.eth.getBalance(a.ethAddress);
        // const ethBalance = await batcher.add(web3.eth.getBalance(a.ethAddress))
        // const ethTransactionCount = web3.eth.getTransactionCount(a.ethAddress);
        return {
          ...a,
          ethBalance: String(ethBalance),
        };
      } catch (error) {
        console.error("Failed to check address", a.ethAddress);
        console.error(error);
        return {
          ...a,
          error: true,
        };
      }
    }),
  );

  // await batcher.execute({
  //   timeout: 0,
  // })
  const results = await resultsPromise;

  await writeFile(
    path.resolve(WORK_DIR, OUTPUT_FILE_NAME),
    JSON.stringify(results, null, 2),
  );
};

main()
  .then(() => {
    console.log(`Checker results saved to "${OUTPUT_FILE_NAME}"`);
    process.exit(0);
  })
  .catch((e) => {
    console.error("Failed to save results", e);
    process.exit(-1);
  });
