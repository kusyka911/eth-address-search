# ETH address search

Script to find ETH address that has some balance. Search based on BIP39 seed-phrase. Will try to generate addresses that may match popular wallets.

## Usage

### prerequisites

- node.js >= 16 installed

### Install dependencies

```sh
npm install
```

### Generate addresses

1. Save your seed phrase to file `_seed.txt` or environment variable

   ```sh
   echo "YOUR SEED" > _seed.txt
   # or
   export SEED_PHRASE="YOUR_SEED"
   ```

   > Security note: you shell will save commands history to `.bash_history` or `.zsh_history` file by default. To avoid this add whitespace at the beginning of the command.

2. run `npm run generate`
   This will create a file `_addresses.json` which will contain ethereum addresses derived from you seed phrase.

> For security reasons it's recommended to generate addresses in secure offline environment.
>
> Best option to linux distribution booted from USB flash drive (live mode). And use second USB flash drive for scripts. After generating `_addresses.json` you should copy this file to a clean flash drive. This file will not contain any private info or executable code that may cause leaks of your private keys.

### Checking addresses balance

0. Copy `_addresses.json` to project directory.

   > In case you generated it in dedicated offline environment

1. Run `npm run check`.
   This will create new file named `_results.json`.
   It will contain info about each address balance. In case of failure it will contain field `"error": true`.

   > Note: Balances will be stored as integer of "gwei".
   >
   > Gwei is just a smaller unit of ETH, just as pennies are to dollars, with the difference being that 1 ETH equals 1 billion gwei. Gwei is useful when talking about very small amounts of ETH.

### ENV

Additional environment variables configuration

```sh
export ETH_RPC_URL="wss://ethereum.publicnode.com"
export DERIVATION_ROTATIONS="5"
```

> `DERIVATION_ROTATIONS` equals to 5 will result to 150 addresses. Increase with caution.

## Milestones

[x] Implement idea

[ ] Make a standalone scripts that will only need node.js to run. Without external dependencies.
